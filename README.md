# build gradle
$ ./gradlew build

# build image
$ docker build --pull --rm -f "Dockerfile" -t filewebflux:latest "."

# Run a container
$ docker run --rm -d  filewebflux:latest

# Run all service
$ docker-compose up -d --build

# Note
You can run direct in you IDE, must modify service name `javaio` to `localhost or 0.0.0.0`
