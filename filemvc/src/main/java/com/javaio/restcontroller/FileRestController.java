package com.javaio.restcontroller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("file")
@Log4j2
public class FileRestController {

//    @Value("${server.port}")
//    String port;

    @Value("${host}")
    String host;

    @PostMapping("/fileOutputStream")
    public ResponseEntity<Map<String, String>> uploadFileOutputStream(
        MultipartFile file
    ) throws IOException {

        long startTime = System.currentTimeMillis();
        String fileName = file.getOriginalFilename();
        long fileSize = file.getSize();

        logMemory("FileOutputStream");
        FileOutputStream out = new FileOutputStream(String.format("files/%s", fileName));
        out.write(file.getBytes());
        logMemory("FileOutputStream");
        out.close();

        Map<String, String> fileRes = new HashMap<String, String>(){{
            put("fileName", fileName);
            put("fileSize", String.valueOf(fileSize));
        }};

        long endTime = System.currentTimeMillis();
        System.out.println("FileOutputStream That took " + (endTime - startTime) + " milliseconds");
        return new ResponseEntity<>(fileRes, HttpStatus.OK);
    }

    @GetMapping("fileInputStream")
    public ResponseEntity<byte[]> downloadFileInputStream(
        @RequestParam("fileName") String fileName
    ) throws IOException {


        long startTime = System.currentTimeMillis();


        logMemory("FileInputStream");
        FileInputStream in = new FileInputStream(String.format("files/%s", fileName));
        byte[] bytes = new byte[in.available()];
        in.read(bytes);
        logMemory("FileInputStream");

        long endTime = System.currentTimeMillis();
        System.out.println("FileInputStream That took " + (endTime - startTime) + " milliseconds");

        return ResponseEntity
                   .ok()
                   .header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s", fileName))
                   .contentLength(bytes.length)
                   .contentType(MediaType.APPLICATION_OCTET_STREAM)
                   .body(bytes);
    }

    @PostMapping("/bufferedOutputStream")
    public ResponseEntity<Map<String, String>> uploadBufferOutputStream(
        MultipartFile file
    ) throws IOException {

        long startTime = System.currentTimeMillis();
        String fileName = file.getOriginalFilename();
        long fileSize = file.getSize();

        logMemory("BufferedOutputStream");
        FileOutputStream out = new FileOutputStream(String.format("files/%s", fileName));
        BufferedOutputStream buffOut = new BufferedOutputStream(out);
        buffOut.write(file.getBytes());
        buffOut.close();
        out.close();
        logMemory("BufferedOutputStream");

        Map<String, String> fileRes = new HashMap<String, String>(){{
            put("fileName", fileName);
            put("fileSize", String.valueOf(fileSize));
        }};

        long endTime = System.currentTimeMillis();
        System.out.println("BufferedOutputStream That took " + (endTime - startTime) + " milliseconds");
        return new ResponseEntity<>(fileRes, HttpStatus.OK);
    }


    @GetMapping("bufferedInputStream")
    public ResponseEntity<byte[]> downloadBufferedInputStream(
        @RequestParam("fileName") String fileName
    ) throws IOException {
        FileInputStream in = new FileInputStream(String.format("files/%s", fileName));
        BufferedInputStream buffIn = new BufferedInputStream(in);

        logMemory("BufferedInputStream");
        byte[] bytes = new byte[buffIn.available()];
        buffIn.read(bytes);
        logMemory("BufferedInputStream");

        return ResponseEntity
                   .ok()
                   .header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s", fileName))
                   .contentLength(bytes.length)
                   .contentType(MediaType.APPLICATION_OCTET_STREAM)
                   .body(bytes);
    }


    @PostMapping
    public ResponseEntity<Map<String, String>> uploadFile(
        MultipartFile file
    ) throws IOException {

        long startTime = System.currentTimeMillis();
        String fileName = file.getOriginalFilename();
        long fileSize = file.getSize();

        logMemory("OutputStream");
//        it will cause Java heap space exception
        Path path = Files.write(Paths.get(String.format("files/%s", fileName)), file.getBytes());
        logMemory("OutputStream");

        Map<String, String> fileRes = new HashMap<String, String>(){{
            put("fileName", path.getFileName().toString());
            put("fileSize", String.valueOf(Files.size(path)));
        }};

        long endTime = System.currentTimeMillis();
        System.out.println("FileOutputStream That took " + (endTime - startTime) + " milliseconds");
        return new ResponseEntity<>(fileRes, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<byte[]> downloadFile(
        @RequestParam("fileName") String fileName
    ) throws IOException {

        long startTime = System.currentTimeMillis();

        logMemory("InputStream");
        byte[] bytes = Files.readAllBytes(Paths.get(String.format("files/%s",fileName)));
        logMemory("InputStream");

        long endTime = System.currentTimeMillis();
        System.out.println("That took " + (endTime - startTime) + " milliseconds");

        return ResponseEntity
               .ok()
               .header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s", fileName))
               .contentLength(bytes.length)
               .contentType(MediaType.APPLICATION_OCTET_STREAM)
               .body(bytes);
    }


    /**
     * restTemplate is used to test uploading a large file
     */
    @PostMapping("largeFile")
    public ResponseEntity<Map<String, String>> uploadLargeFile(
        MultipartFile file
    ) throws IOException {

        long startTime = System.currentTimeMillis();
        logMemory("UploadLargeFile");
        String fileName = file.getOriginalFilename();

        RestTemplate restTemplate = new RestTemplate();

        /**
         *  Enable to upload a large file.
         * Indicate whether this request factory should buffer the
         * {@linkplain ClientHttpRequest#getBody() request body} internally.
         * <p>Default is {@code true}. When sending large amounts of data via POST or PUT,
         * it is recommended to change this property to {@code false}, so as not to run
         * out of memory. This will result in a {@link ClientHttpRequest} that either
         * streams directly to the underlying
         */
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        requestFactory.setBufferRequestBody(false);
        restTemplate.setRequestFactory(requestFactory);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> part = new LinkedMultiValueMap<>();

        /**
         *  This code will cause java heap because it will store the upload file in memory before request to the request endpoint
         */
//        part.add("file", new InputStreamResource(file.getInputStream(), fileName));

        /**
         * These 2 ways below solve upload a large file because its multipart body is not stored in buff (memory)
         */
        part.add("file", new MultipartInputStreamFileResource(file.getInputStream(), file.getOriginalFilename()));

//        file.transferTo(Paths.get(String.format("files/%s", fileName)));
//        part.add("file", new FileSystemResource(new File(String.format("files/%s", fileName)).getPath()));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(part, headers);

        ResponseEntity<Map> reponseEntity = restTemplate.exchange(
            String.format("http://%s:%s/file/largeFileRestTemplate", host, "9091"),
            HttpMethod.POST,
            requestEntity,
            Map.class
        );
        Map response = reponseEntity.getBody();

        logMemory("UploadLargeFile");

        Map<String, String> fileRes = new HashMap<String, String>(){{
            put("fileName", String.valueOf(response.get("fileName")));
            put("fileSize", String.valueOf(response.get("fileSize")));
        }};

        long endTime = System.currentTimeMillis();
        System.out.println("FileOutputStream That took " + (endTime - startTime) + " milliseconds");
        return new ResponseEntity<>(fileRes, HttpStatus.OK);
    }

    /**
     * restTemplate is used to test download a large file
     */
    @GetMapping("largeFile")
    public ResponseEntity<InputStreamResource> downloadLargeFile(
            @RequestParam("fileName") String fileName
    ) throws IOException {

        long startTime = System.currentTimeMillis();
        logMemory("DownloadLargeFile");

        RestTemplate restTemplate = new RestTemplate();

        // Optional Accept header
        RequestCallback requestCallback = request -> request.getHeaders()
                .setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM, MediaType.ALL));

        // Streams the response instead of loading it all in memory
        ResponseExtractor<InputStreamResource> responseExtractor = response -> {
            // Here I write the response to a file but do what you like
            Path path = Paths.get("tmp/" + fileName);
            Files.copy(response.getBody(), path, StandardCopyOption.REPLACE_EXISTING);
            return new InputStreamResource(new FileInputStream(String.format("tmp/%s", fileName)));
        };

        InputStreamResource response = restTemplate.execute(
            String.format("http://%s:%s/file/largeFileRestTemplate?fileName=%s", host, "9091", fileName),
            HttpMethod.GET,
            requestCallback,
            responseExtractor
        );

        logMemory("LargeFile");

        long endTime = System.currentTimeMillis();
        System.out.println("FileOutputStream That took " + (endTime - startTime) + " milliseconds");
        return ResponseEntity
            .ok()
            .header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s", fileName))
            .body(response);
    }


    /**
     * API is requested by a restTemplate,
     * This API is used to handle upload large files
     */
    @PostMapping("largeFileRestTemplate")
    public ResponseEntity<Map<String, String>> uploadLargeFileRestTemplate(
        MultipartFile file
    ) throws IOException {

        long startTime = System.currentTimeMillis();
        String fileName = file.getOriginalFilename();
        long fileSize = file.getSize();

        logMemory("uploadLargeFileRestTemplate");
        file.transferTo(Paths.get(String.format("files/%s", fileName)));
        logMemory("uploadLargeFileRestTemplate");

        Map<String, String> fileRes = new HashMap<String, String>(){{
            put("fileName", fileName);
            put("fileSize", String.valueOf(fileSize));
        }};

        long endTime = System.currentTimeMillis();
        System.out.println("FileOutputStream That took " + (endTime - startTime) + " milliseconds");
        return new ResponseEntity<>(fileRes, HttpStatus.OK);
    }


    /**
     * This API is used to handle download large files
     */
    @GetMapping("largeFileRestTemplate")
    public ResponseEntity<InputStreamResource> downloadLargeFileRestTemplate(
            @RequestParam("fileName") String fileName
    ) throws IOException {

        long startTime = System.currentTimeMillis();

        logMemory("downloadLargeFileRestTemplate");
        InputStreamResource inputStreamResource = new InputStreamResource(new FileInputStream(String.format("files/%s", fileName)));
        logMemory("downloadLargeFileRestTemplate");

        long endTime = System.currentTimeMillis();
        System.out.println("FileInputStream That took " + (endTime - startTime) + " milliseconds");
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=%s", fileName))
                .body(inputStreamResource);
    }

    /**
     * Testing API
     */
    @PostMapping("Hello")
    public Map<String, String> hello(){
        return new HashMap<String, String>(){{put("fileName", "Hello1"); put("fileSize", "Hello2");}};
    }

    class MultipartInputStreamFileResource extends InputStreamResource {

        private final String filename;

        MultipartInputStreamFileResource(InputStream inputStream, String filename) {
            super(inputStream);
            this.filename = filename;
        }

        @Override
        public String getFilename() {
            return this.filename;
        }

        @Override
        public long contentLength() throws IOException {
            return -1; // we do not want to generally read the whole stream into memory ...
        }
    }

    private final void logMemory() {
        logMemory("");
    }

    private final void logMemory(String method) {
        log.info(method + " Max Memory: {} Mb", Runtime.getRuntime().maxMemory() / 1048576);
        log.info(method + " Total Memory: {} Mb", Runtime.getRuntime().totalMemory() / 1048576);
        log.info(method + " Free Memory: {} Mb \n", Runtime.getRuntime().freeMemory() / 1048576);
    }

}
