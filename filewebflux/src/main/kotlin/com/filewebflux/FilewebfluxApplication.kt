package com.filewebflux

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.core.SpringVersion

@SpringBootApplication
class FilewebfluxApplication

fun main(args: Array<String>) {
	println("Version: ${SpringVersion.getVersion()}")
	runApplication<FilewebfluxApplication>(*args)
}
