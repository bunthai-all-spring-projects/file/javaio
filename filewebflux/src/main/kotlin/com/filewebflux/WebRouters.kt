package com.filewebflux

import kotlinx.coroutines.reactive.awaitFirst
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.FileSystemResource
import org.springframework.core.io.InputStreamResource
import org.springframework.http.*
import org.springframework.http.codec.multipart.FilePart
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import org.springframework.web.reactive.function.client.awaitExchange
import org.springframework.web.reactive.function.server.*
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.util.HashMap

@Configuration
class WebRouters() {

    @Bean
    fun html() = coRouter {
        contentType(MediaType.MULTIPART_FORM_DATA).nest {
            POST("/files") { request ->
                val multipartFormData = request.awaitMultipartData()

                logMemory("File")

                var file = multipartFormData["file"]?.first() ?: return@POST ServerResponse.badRequest().buildAndAwait()


                /**
                 *  it will cause java heap because it will store the upload file in memory before request to the request endpoint
                 */
                val map: MultiValueMap<String, Any> = LinkedMultiValueMap();
                var filePart = file;
                filePart = filePart as FilePart
                val fileName = filePart.filename()
                filePart.transferTo(File("/tmp/$fileName"))
                map.set("file", FileSystemResource("/tmp/$fileName"))

                // put multipart direct
//                map.set("file", file)

                val responseEntity: Any = WebClient.create().post().uri("http://javaio:9091/file/largeFileRestTemplate")
                    .contentType(MediaType.MULTIPART_FORM_DATA)
                    .body(BodyInserters.fromMultipartData(map))
                    .awaitExchange().awaitBody()



                /**
                 * Upload large will be streamed direct to the request endpoint, it will not land file into memory
                 */
////                file = file as FilePart
//                val startTime = System.currentTimeMillis()
//                val fileName: String = file.name()
//
//                val restTemplate = RestTemplate()
//
//                val headers = HttpHeaders()
//                headers.contentType = MediaType.MULTIPART_FORM_DATA
//
//                val part = LinkedMultiValueMap<String, Any>()
//
//
////                part.add("file", file)
//                var filePart = file;
//                filePart = filePart as FilePart
//                filePart.transferTo(File("/tmp/$fileName"))
//                part.add("file", FileSystemResource("/tmp/$fileName"))
////                part.add("file", MultipartInputStreamFileResource(file.content().awaitFirst().asInputStream(), file.name()))
//
//                val requestEntity = HttpEntity(part, headers)
//
//                val responseEntity: Any = restTemplate.exchange<Any>(
//                        "http://javaio:9091/file/largeFileRestTemplate",
//                        HttpMethod.POST,
//                        requestEntity,
//                        Any().javaClass
//                ).body!!

                logMemory("File")
                ServerResponse.ok().bodyValueAndAwait(responseEntity)
            }
        }
    }

    internal class MultipartInputStreamFileResource(inputStream: InputStream?, private val filename: String) : InputStreamResource(inputStream!!) {
        override fun getFilename(): String? {
            return filename
        }

        @Throws(IOException::class)
        override fun contentLength(): Long {
            return -1 // we do not want to generally read the whole stream into memory ...
        }

    }

    private fun logMemory(method: String) {
        println("$method Max Memory: ${Runtime.getRuntime().maxMemory() / 1048576} Mb")
        println("$method Total Memory: ${Runtime.getRuntime().totalMemory() / 1048576} Mb")
        println("$method Free Memory: ${Runtime.getRuntime().freeMemory() / 1048576} Mb \n")
    }
}